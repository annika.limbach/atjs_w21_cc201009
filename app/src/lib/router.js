import { createRouter, createWebHistory } from 'vue-router';

import Home from '@/pages/Home.vue';
import Calculator from '@/pages/Calculator.vue';
import Pokedex from '@/pages/Pokedex.vue';
import Guesser from '@/pages/Guesser.vue';
import Cardgame from '@/pages/Cardgame.vue';

const routes = [
  { path: '/', component: Home },
  { path: '/calculator', component: Calculator },
  { path: '/pokedex', component: Pokedex },
  { path: '/guesser', component: Guesser },
  { path: '/cardgame', component: Cardgame },
];

// Here we create our own Vue Router Instance
// and define the paths we can then use.
const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
